import 'package:cloud_firestore/cloud_firestore.dart';

class BMI {
  BMI({this.dateTime, this.height, this.weight});

  final Timestamp dateTime;
  final num height;
  final num weight;
}