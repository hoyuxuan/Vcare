class User {
  final String uid;

  User({this.uid});
}

class UserDisease {
  final bool arthritis;
  final bool asthma;
  final bool alzheimer;
  final bool cancer;
  final bool diabetes;
  final bool hbp;
  final bool hc;
  final bool heartDisease;
  final bool obesity;
  final bool osteoporosis;

  UserDisease(
      {this.arthritis,
      this.asthma,
      this.alzheimer,
      this.cancer,
      this.diabetes,
      this.hbp,
      this.hc,
      this.heartDisease,
      this.obesity,
      this.osteoporosis});
}

class UserDataBMI {
  final String uid;
  final num height;
  final num weight;
  final String bmiTag;

  UserDataBMI({this.uid, this.height, this.weight, this.bmiTag});
}

class UserDataBP {
  final String uid;
  final String hbpTag;
  final num sys;
  final num dia;
  final num pul;

  UserDataBP({this.uid, this.hbpTag, this.sys, this.dia, this.pul});
}


class UserDataBG {
  final String uid;
  final String bgTag;
  final num bg;
  final String mealtime;


  UserDataBG({this.uid, this.bgTag, this.bg,this.mealtime});
}

class UserDataBMIList {
  String uid;
  String bmiTag;
  num height;
  num weight;
  DateTime dateTime;

  UserDataBMIList({this.uid, this.bmiTag, this.height, this.weight, this.dateTime});

  UserDataBMIList.fromMap(Map snapshot,String uid):
        uid = snapshot['uid'] ?? ' ',
        bmiTag = snapshot['bmiTag'],
        height = snapshot['height'],
        weight = snapshot['weight'],
        dateTime = snapshot['dateTime'];

}


class UserDataBPList {
   String uid;
   String hbpTag;
   num sys;
   num dia;
   num pul;
   DateTime dateTime;

  UserDataBPList({this.uid, this.hbpTag, this.sys, this.dia, this.pul, this.dateTime});

  UserDataBPList.fromMap(Map snapshot,String uid):
        uid = snapshot['uid'] ?? ' ',
        hbpTag = snapshot['hbpTag'],
   sys = snapshot['sys'],
   dia = snapshot['dia'],
   pul = snapshot['pul'],
   dateTime = snapshot['dateTime'];

}



class UserAllergy {
  final bool Dairy;
  final bool Egg;
  final bool Gluten;
  final bool Grain;
  final bool Peanut;
  final bool Seafood;
  final bool Sesame;
  final bool Shellfish;
  final bool Soy;
  final bool Sulfite;
  final bool Tree_Nut;
  final bool Wheat;

  UserAllergy({
    this.Dairy,
    this.Egg,
    this.Gluten,
    this.Grain,

    this.Peanut,
    this.Seafood,
    this.Sesame,
    this.Shellfish,
    this.Soy,
    this.Sulfite,
    this.Tree_Nut,
    this.Wheat,
  });
}
