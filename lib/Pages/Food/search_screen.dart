import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vcare/models/meal_plan_model.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/api_services.dart';
import 'package:vcare/Pages/Food/meals_screen.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/loading.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<String> _diets = [
    'None',
    'Gluten Free',
    'Ketogenic',
    'Lacto-Vegetarian',
    'Ovo-Vegetarian',
    'Vegan',
    'Pescetarian',
    'Paleo',
    'Primal',
    'Whole30',
  ];

  double _targetCalories = 2250;
  String _diet = 'None';

  List<String> intolerance = [];

  void _searchMealPlan() async {
    MealPlan mealPlan = await APIService.instance.generateMealPlan(
      targetCalories: _targetCalories.toInt(),
      diet: _diet,
      intolerances: intolerance.toString(),
    );
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => MealsScreen(mealPlan: mealPlan),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue[900],
        title: Text(
          'Your Meal',
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
        centerTitle: true,
//            elevation: 0,
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
              'https://firebasestorage.googleapis.com/v0/b/vcare-d06ff.appspot.com/o/800px_COLOURBOX20304186.jpg?alt=media&token=620d3f7a-227a-4ece-a2d3-93c94b8c2135',
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 30.0,
            ),
            padding: EdgeInsets.symmetric(horizontal: 30.0),
            height: MediaQuery.of(context).size.height * 0.75,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.9),
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Daily Meal Planner',
                  style: TextStyle(
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2.0,
                  ),
                ),
                SizedBox(height: 20.0),
                RichText(
                  text: TextSpan(
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(fontSize: 25),
                    children: [
                      TextSpan(
                        text: _targetCalories.truncate().toString(),
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextSpan(
                        text: ' cal',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
                SliderTheme(
                  data: SliderTheme.of(context).copyWith(
                    thumbColor: Theme.of(context).primaryColor,
                    activeTrackColor: Theme.of(context).primaryColor,
                    inactiveTrackColor: Colors.lightBlue[100],
                    trackHeight: 6.0,
                  ),
                  child: Slider(
                    min: 0.0,
                    max: 4500.0,
                    value: _targetCalories,
                    onChanged: (value) => setState(() {
                      _targetCalories = value.round().toDouble();
                    }),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: [
                      StreamBuilder<UserAllergy>(
                          stream: DatabaseService(uid: user.uid).userAllergy,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              UserAllergy userAllergy = snapshot.data;

                              if (userAllergy.Dairy == true) {
                                if (intolerance.contains('Dairy'))
                                  intolerance.remove('Dairy');
                                intolerance.add('Dairy');
                              }
                              if (userAllergy.Egg == true) {
                                if (intolerance.contains('Egg'))
                                  intolerance.remove('Egg');
                                intolerance.add('Egg');
                              }
                              if (userAllergy.Gluten == true) {
                                if (intolerance.contains('Gluten'))
                                  intolerance.remove('Gluten');
                                intolerance.add('Gluten');
                              }
                              if (userAllergy.Grain == true) {
                                if (intolerance.contains('Grain'))
                                  intolerance.remove('Grain');
                                intolerance.add('Grain');
                              }
                              if (userAllergy.Peanut == true) {
                                if (intolerance.contains('Peanut'))
                                  intolerance.remove('Peanut');
                                intolerance.add('Peanut');
                              }
                              if (userAllergy.Seafood == true) {
                                if (intolerance.contains('Seafood'))
                                  intolerance.remove('Seafood');
                                intolerance.add('Seafood');
                              }
                              if (userAllergy.Sesame == true) {
                                if (intolerance.contains('Sesame'))
                                  intolerance.remove('Sesame');
                                intolerance.add('Sesame');
                              }
                              if (userAllergy.Shellfish == true) {
                                if (intolerance.contains('Shellfish'))
                                  intolerance.remove('Shellfish');
                                intolerance.add('Shellfish');
                              }
                              if (userAllergy.Soy == true) {
                                if (intolerance.contains('Soy'))
                                  intolerance.remove('Soy');
                                intolerance.add('Soy');
                              }
                              if (userAllergy.Sulfite == true) {
                                if (intolerance.contains('Sulfite'))
                                  intolerance.remove('Sulfite');
                                intolerance.add('Sulfite');
                              }
                              if (userAllergy.Tree_Nut == true) {
                                if (intolerance.contains('Tree Nut'))
                                  intolerance.remove('Tree Nut');
                                intolerance.add('Tree Nut');
                              }
                              if (userAllergy.Wheat == true) {
                                if (intolerance.contains('Wheat'))
                                  intolerance.remove('Wheat');
                                intolerance.add('Wheat');
                              }
                              print(intolerance.toString());

                              return SizedBox(height: 10);
                            } else
                              print("error fetch Intolerence List");
                            return Loading();
                          }),
                      DropdownButtonFormField(
                        items: _diets.map((String priority) {
                          return DropdownMenuItem(
                            value: priority,
                            child: Text(
                              priority,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                              ),
                            ),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          labelText: 'Diet',
                          labelStyle: TextStyle(fontSize: 18.0),
                        ),
                        onChanged: (value) {
                          setState(() {
                            _diet = value;
                          });
                        },
                        value: _diet,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30.0),
                FlatButton(
                  padding: EdgeInsets.symmetric(
                    horizontal: 60.0,
                    vertical: 8.0,
                  ),
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Text(
                    'Search',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  onPressed: _searchMealPlan,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
