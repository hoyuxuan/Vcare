import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:vcare/Pages/MedicalReport/editAllergyCheckbox.dart';
import 'package:vcare/Pages/MedicalReport/editDiseasesCheckbox.dart';
import 'package:vcare/services/database.dart';
import 'package:flutter/material.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/models/user.dart';

class MedicalReport extends StatefulWidget {
  @override
  _MedicalReportState createState() => _MedicalReportState();
}

class _MedicalReportState extends State<MedicalReport> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        backgroundColor: Colors.lightBlue[900],
        title: Text(
          'Your Medical Report',
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
        centerTitle: true,
//            elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.grey[100],
          child: Column(
            children: [
              Selection_Bar(),
              SizedBox(height: 30,),
              Text("Manage your health records",style: TextStyle(fontSize: 20, )),
              bmicard(),
              hbpcard(),
              bgcard(),
            ],
          ),
        ),
      ),
    );
  }
}

class Selection_Bar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void _showCurrentDiseaseCheckbox() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30),
              child: EditDiseasesCheckbox(),
            );
          },
          isScrollControlled: true,
          useRootNavigator: true);
    }

    void _showAllergyCheckbox() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30),
              child: EditAllergyCheckbox(),
            );
          },
          isScrollControlled: true,
          useRootNavigator: true);
    }

    return Row(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: GestureDetector(
            onTap: () => _showCurrentDiseaseCheckbox(),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                gradient: new LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Color.fromARGB(255, 0, 146, 255),
                    Color.fromARGB(255, 27, 187, 255)
                  ],
                ),
              ),
              margin: EdgeInsets.fromLTRB(20.0, 10.0, 5.0, 0.0),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text("Current Disease",
                    style: TextStyle(fontSize: 22, color: Colors.white)),
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: GestureDetector(
            onTap: () => _showAllergyCheckbox(),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                gradient: new LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Color.fromARGB(255, 0, 146, 255),
                    Color.fromARGB(255, 27, 187, 255)
                  ],
                ),
              ),
              margin: EdgeInsets.fromLTRB(5.0, 10.0, 20.0, 0.0),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text("Food Allergy",
                    style: TextStyle(fontSize: 22, color: Colors.white)),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class bmicard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDataBMI>(
      stream: DatabaseService(uid: user.uid).userBMI,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, '/viewBMITrend'),
                    child: ListTile(
                      subtitle: Text(
                        "Record your Body Mass Index (kg/m\u00B2)",
                        style: TextStyle(fontSize: 22,color: Colors.black87),
                      ),
                      trailing: Icon(
                        Icons.edit,
                        size: 30,
                        color: Colors.blue[800],
                      ),
                      title: Text(
                        'BMI ',
                        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
                      ),
                      contentPadding: EdgeInsets.all(15.0),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Loading();
        }
      },
    );
  }
}

class hbpcard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDataBP>(
      stream: DatabaseService(uid: user.uid).userBP,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, '/viewBPTrend'),
                    child: ListTile(
                      subtitle: Text(
                        "Record the pressure of your blood",
                        style: TextStyle(fontSize: 22,color: Colors.black87),
                      ),
                      trailing: Icon(
                        Icons.edit,
                        size: 30,
                        color: Colors.blue[700],
                      ),
                      title: Text(
                        'Blood Pressure ',
                        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
                      ),
                      contentPadding: EdgeInsets.all(15.0),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Loading();
        }
      },
    );
  }
}

class bgcard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDataBG>(
      stream: DatabaseService(uid: user.uid).userBG,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, '/viewBGTrend'),
                    child: ListTile(
                      subtitle: Text(
                        "Record the amount of glucose in your blood",
                        style: TextStyle(fontSize: 22,color: Colors.black87),
                      ),
                      trailing: Icon(
                        Icons.edit,
                        size: 30,
                        color: Colors.blue[700],
                      ),
                      title: Text(
                        'Blood Glucose',
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.w700),
                      ),
                      contentPadding: EdgeInsets.all(15.0),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Loading();
        }
      },
    );
  }
}
