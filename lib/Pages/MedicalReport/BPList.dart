import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:vcare/models/Calculation.dart';
import 'package:vcare/models/user.dart';
import 'package:intl/intl.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/constants.dart';

class BPList extends StatefulWidget {
  @override
  _BPListState createState() => _BPListState();
}

class _BPListState extends State<BPList> {
  navigateToDetail(DocumentSnapshot post) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailPage(
                  post: post,
                )));
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    Future getPost() async {
      var fs = Firestore.instance;
      Query q = fs
          .collection('UserData')
          .document(user.uid)
          .collection('med_report')
          .document('UserRecords')
          .collection('BP_List')
          .orderBy('dateTime', descending: true);
      QuerySnapshot qn = await q.getDocuments();
      return qn.documents;
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[900],
          title: Text(
            "Past B.Pressure Records",
            style: TextStyle(color: Colors.white, fontSize: 25),
          ),
        ),
        body: Container(
            child: FutureBuilder(
                future: getPost(),
                builder: (_, snapshot) {
                  return ListView.builder(
                      itemCount: snapshot.data.length - 1,
                      itemBuilder: (_, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListTile(
                            onTap: () =>
                                navigateToDetail(snapshot.data[index + 1]),
                            title: Text(
                              snapshot.data[index + 1].data['sys'].toString() +
                                  " / " +
                                  snapshot.data[index + 1].data['dia'].toString() +
                                  " mmHg",
                              style: TextStyle(fontSize: 25),
                            ),
                            subtitle: Text(
                              DateFormat.MMMd().add_jm().format(snapshot
                                  .data[index + 1].data['dateTime']
                                  .toDate()),
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        );
                      });
                })));
  }
}

class showAlertDialog extends StatefulWidget {
  final Data data;

  showAlertDialog({this.data});

  @override
  _showAlertDialogState createState() => _showAlertDialogState();
}

class _showAlertDialogState extends State<showAlertDialog> {
  Timestamp dateTime;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    CollectionReference BPlistRef = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BP_List');

    dateTime = widget.data.dateTime;

    Future deleteBPRecord(context) async {
      return await BPlistRef.document(dateTime.toString()).delete();
    }

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel",style: TextStyle(fontSize: 23),),
      onPressed: () {
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Delete",style: TextStyle(fontSize: 23),),
      onPressed: () {
        deleteBPRecord(context);
        Navigator.of(context, rootNavigator: true).pop();
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 3;
        });
      },
    );
    SchedulerBinding.instance.addPostFrameCallback((_) {
      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Delete this record?",style: TextStyle(fontSize: 25),),
        actions: [
          cancelButton,
          continueButton,
        ],
      );
      // show the dialog

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    });
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white24,
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  final DocumentSnapshot post;

  DetailPage({this.post});

  @override
  _DetailPageState createState() => _DetailPageState();
}

class Data {
  num sys;
  num dia;
  num pul;
  String tag;
  Timestamp dateTime;

  Data({this.sys, this.dia, this.pul, this.tag, this.dateTime});
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    final data = Data(
      sys: widget.post.data['sys'],
      dia: widget.post.data['dia'],
      pul: widget.post.data['pul'],
      tag: widget.post.data['hbpTag'],
      dateTime: widget.post.data['dateTime'],
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        elevation: 0.0,
        actions: <Widget>[
          Row(
            children: [
              FlatButton.icon(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EditBPForm2(
                                data: data,
                              )));
                },
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                label: Text(
                  'Edit',
                  style: TextStyle(fontSize:23,color: Colors.white),
                ),
              ),
              FlatButton.icon(
                onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => showAlertDialog(
                                data: data,
                              )))
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
                label: Text(
                  'Delete',
                  style: TextStyle(fontSize:23,color: Colors.white),
                ),
              ),
            ],
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(children: [
          SizedBox(
            height: 30,
          ),
          Text(
            DateFormat.yMMMMd('en_US')
                .add_jm()
                .format(widget.post.data['dateTime'].toDate()),
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 50,
          ),
          Wrap(
            alignment: WrapAlignment.center,
            children: [
              Container(
                child: Text(
                  widget.post.data['hbpTag'],
                  style: TextStyle(fontSize: 50, fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                        widget.post.data['sys'].toString() +
                            " / " +
                            widget.post.data['dia'].toString(),
                        style: TextStyle(fontSize: 30)),
                    Text(" mmHg", style: TextStyle(fontSize: 20)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                        widget.post.data['pul'].toString(),
                        style: TextStyle(fontSize: 30)),
                    Text(" bpm", style: TextStyle(fontSize: 20)),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 25,
          ),
        ]),
      ),
    );
  }
}

class EditBPForm2 extends StatefulWidget {
  final Data data;

  EditBPForm2({this.data});

  @override
  _EditBPForm2State createState() => _EditBPForm2State();
}

class _EditBPForm2State extends State<EditBPForm2> {
  final _formKey = GlobalKey<FormState>();

  num _currentsys;
  num _currentdia;
  num _currentpul;
  String _currentBpTag;
  num _initcurrentsys;
  num _initcurrentdia;
  num _initcurrentpul;

  Timestamp _initrecordTimestamp;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    
    _initcurrentsys = widget.data.sys;
    _initcurrentdia = widget.data.dia;
    _initcurrentpul = widget.data.pul;
    _initrecordTimestamp = widget.data.dateTime;

    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Edit record",
            style: TextStyle(fontSize: 25),
          ),
          backgroundColor: Colors.blue[900],
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(22.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    'Systolic Blood Pressure (SYS)',
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: _initcurrentsys.toStringAsFixed(0),
                    decoration:
                        textInputDecoration.copyWith(hintText: 'SYS Value'),
                    keyboardType: TextInputType.number,
                    // ignore: missing_return
                    validator: (val) {
                      if (val.length == 0 ||
                          double.parse(val) == 0.0 ||
                          double.parse(val) < 70.0) {
                        return ('Error , SYS value should more than 70mmHg');
                      }
                    },
                    onChanged: (val) =>
                        setState(() => _currentsys = int.parse(val)),
                  ),
                  SizedBox(
                    height: 50.0,
                  ),
                  Text(
                    'Diastolic Blood Pressure (DIA)',
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: _initcurrentdia.toStringAsFixed(0),
                    keyboardType: TextInputType.number,
                    decoration:
                        textInputDecoration.copyWith(hintText: 'DIA value '),
                    // ignore: missing_return
                    validator: (value) {
                      if (value.length == 0 ||
                          double.parse(value) == 0.0 ||
                          double.parse(value) < 40.0) {
                        return ('Error , DIA value should more than 40mmHg');
                      }
                    },
                    onChanged: (val) =>
                        setState(() => _currentdia = int.parse(val)),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    'Heart Rate (bpm)',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: _initcurrentpul.toString(),
                    decoration: textInputDecoration.copyWith(
                        hintText: 'your heart rate/ minute'),
                    keyboardType: TextInputType.number,
                    // ignore: missing_return
                    validator: (val) {
                      if (val.length == 0 ||
                          double.parse(val) == 0.0 ||
                          double.parse(val) < 40.0) {
                        return ('Error , your pulse value should more than 40pulse/minute');
                      }
                    },
                    onChanged: (val) =>
                        setState(() => _currentpul = int.parse(val)),
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  RaisedButton(
                      color: Colors.lightBlueAccent,
                      child: Text(
                        'Update',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          if (_currentsys == null) _currentsys = _initcurrentsys;
                          if (_currentdia == null) _currentdia = _initcurrentdia;
                          if (_currentpul == null) _currentpul = _initcurrentpul;

                          _currentBpTag =
                              Calculation.bpCal(_currentsys, _currentdia);
                          DocumentReference tagCollection = Firestore.instance
                              .collection('Tag')
                              .document(user.uid);
                          DocumentSnapshot tagList = await tagCollection.get();
                          List Tags = tagList.data['Tags'];
                          if (Tags.contains('N/A') ||
                              Tags.contains('Low BP') ||
                              Tags.contains('Normal BP') ||
                              Tags.contains('HBP')) {
                            if (Tags.contains('N/A'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['N/A'])
                              });
                            if (Tags.contains('Low BP'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['Low BP'])
                              });
                            if (Tags.contains('Normal BP'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['Normal BP'])
                              });
                            if (Tags.contains('HBP'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['HBP'])
                              });
                            if (Tags.contains('Abnormal Value'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['Abnormal Value'])
                              });
                            if (_currentBpTag == "High-normal BP" ||
                                _currentBpTag == "High BP - Severe" ||
                                _currentBpTag ==
                                    "Isolated Systolic Hypertension") {
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayUnion(["HBP"])
                              });
                            } else {
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayUnion([_currentBpTag])
                              });
                            }
                          } else {
                            if (_currentBpTag == "High-normal BP" ||
                                _currentBpTag == "High BP - Severe" ||
                                _currentBpTag ==
                                    "Isolated Systolic Hypertension") {
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayUnion(["HBP"])
                              });
                            } else {
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayUnion([_currentBpTag])
                              });
                            }
                          }

                          await DatabaseService(uid: user.uid).updateBPList(
                              _currentBpTag,
                              int.parse(_currentsys.toStringAsFixed(0)) ??
                                  _initcurrentsys,
                              int.parse(_currentdia.toStringAsFixed(0)) ??
                                  _initcurrentdia,
                              int.parse(_currentpul.toStringAsFixed(0)) ??
                                  _initcurrentpul,
                              _initrecordTimestamp);

                          int count = 0;
                          Navigator.popUntil(context, (route) {
                            return count++ == 3;
                          });
                        } else {
                          print('value error ! cannot save!');
                        }
                      }),
                ],
              ),
            ),
          ),
        ));
  }
}
