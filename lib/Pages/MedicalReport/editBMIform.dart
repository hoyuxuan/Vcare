import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/constants.dart';
import 'package:provider/provider.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/models/Calculation.dart';

class EditBMIForm extends StatefulWidget {
  @override
  _EditBMIFormState createState() => _EditBMIFormState();
}

class _EditBMIFormState extends State<EditBMIForm> {

  final _formKey = GlobalKey<FormState>();

//form value
  double _currentHeight;
  double _currentWeight;
  String _currentBmiTag;
  double _initcurrentHeight;
  double _initcurrentWeight;
  String _initcurrentBmiTag;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDataBMI>(
        stream: DatabaseService(uid: user.uid).userBMI,
        builder: (context, snapshot) {

          if(snapshot.hasData){
            UserDataBMI userDataBMI = snapshot.data;
            _initcurrentBmiTag=userDataBMI.bmiTag;
            _initcurrentHeight=userDataBMI.height;
            _initcurrentWeight=userDataBMI.weight;
            return Form(
              key: _formKey,
              child: Column(
                children: [
                  Text(
                    'Add latest record',
                    style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.w800),
                  ),
                  SizedBox(height: 30.0,),
                  Text('Your Height (cm)',style: TextStyle(fontSize: 22),),
                  SizedBox(height:10.0,),
                  TextFormField(
                      style: TextStyle(fontSize: 22),
                    initialValue: userDataBMI.height.toStringAsFixed(2),
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Enter your Height'
                    ),
                    keyboardType: TextInputType.number,
                    // ignore: missing_return
                    validator:  (val) {
                      if (val.length == 0 || double.parse(val) == 0.0 || double.parse(val) < 100.0) {
                        return ('Height should be more then 100.0 or is a Number');
                      }
                    },
                    onChanged: (val)=> setState(() =>_currentHeight = double.parse(val)),
                  ),
                  SizedBox(height: 50.0,),
                  Text('Your Weight (kg)',style: TextStyle(fontSize: 22),),
                  SizedBox(height:10.0,),
                  TextFormField(
                      style: TextStyle(fontSize: 22),
                    initialValue: userDataBMI.weight.toStringAsFixed(1),
                    keyboardType: TextInputType.number,
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Enter your Weight'
                    ),
                    // ignore: missing_return
                    validator: (value) {
                      if (value.length == 0 || double.parse(value) == 0.0 || double.parse(value) < 20.0) {
                        return ('Weight should be more then 20.0 or is a Number');
                      }
                    },
                    onChanged: (val)=> setState(() =>_currentWeight = double.parse(val)
                    ),
                  ),
                  SizedBox(height: 25.0,),
                  RaisedButton(
                      color: Colors.lightBlueAccent,
                      child: Text(
                        'Add',style: TextStyle(fontSize: 20,color: Colors.white),
                      ),
                      onPressed: ()async{

                        if (_formKey.currentState.validate()){

                          if(_currentWeight==null)
                            _currentWeight=_initcurrentWeight;
                          if(_currentHeight==null)
                            _currentHeight=_initcurrentHeight;

                          _currentBmiTag= Calculation.bmiCal(_currentHeight, _currentWeight);
                          DocumentReference tagCollection = Firestore.instance.collection('Tag').document(user.uid);
                          DocumentSnapshot tagList  = await tagCollection.get();
                          List Tags = tagList.data['Tags'];
                          if(Tags.contains('N/A')||Tags.contains('Overweight')||Tags.contains('Underweight')||Tags.contains('Normal Weight')){
                            if(Tags.contains('N/A'))
                              tagCollection.updateData(
                                  {'Tags' : FieldValue.arrayRemove(['N/A'])}
                              );
                            if(Tags.contains('Overweight'))
                              tagCollection.updateData(
                                  {'Tags' : FieldValue.arrayRemove(['Overweight'])}
                              );
                            if(Tags.contains('Normal Weight'))
                              tagCollection.updateData(
                                  {'Tags' : FieldValue.arrayRemove(['Normal Weight'])}
                              );
                            if(Tags.contains('Underweight'))
                              tagCollection.updateData(
                                  {'Tags' : FieldValue.arrayRemove(['Underweight'])}
                              );
                            tagCollection.updateData(
                                {'Tags' : FieldValue.arrayUnion([_currentBmiTag])}
                            );
                          }else{
                            tagCollection.updateData(
                                {'Tags' : FieldValue.arrayUnion([_currentBmiTag])}
                            );
                          }
                          await DatabaseService(uid: user.uid)
                              .updateBMI(
                              _currentBmiTag ?? _initcurrentBmiTag,
                              double.parse(_currentHeight.toStringAsFixed(2)) ?? _initcurrentHeight,
                              double.parse(_currentWeight.toStringAsFixed(1)) ?? _initcurrentWeight
                          );
                          await DatabaseService(uid: user.uid).updateBMIList(
                              _currentBmiTag,
                              double.parse(_currentHeight.toStringAsFixed(2)),
                              double.parse(_currentWeight.toStringAsFixed(1)),
                              Timestamp.now());
                          Navigator.pop(context);
                        }else{
                          print('value error ! cannot save!');
                        }
                      }
                  ),
                  SizedBox(height: 280,),
                  Text("Swipe down to close",style: TextStyle(
                      fontSize: 20,color: Colors.black45
                  ),)
                ],
              ),
            );
          }else{
            return Loading();
          }

        }
    );
  }
}