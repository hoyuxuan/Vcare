import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/constants.dart';
import 'package:provider/provider.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/models/Calculation.dart';

class EditBGForm extends StatefulWidget {
  @override
  _EditBGFormState createState() => _EditBGFormState();
}

class _EditBGFormState extends State<EditBGForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> mealstatus = ['Fasting', 'Non-Fasting'];

//form value
  num _currentbg;
  String _currentBgTag;
  String _currentMealStatus;
  num _initcurrentbg;
  String _initcurrentBgTag;
  String _initMealStatus;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDataBG>(
        stream: DatabaseService(uid: user.uid).userBG,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserDataBG userDataBG = snapshot.data;
            _initcurrentbg = userDataBG.bg;
            _initcurrentBgTag = userDataBG.bgTag;
            _initMealStatus = userDataBG.mealtime;
            return Form(
              key: _formKey,
              child: Column(
                children: [
                  Text(
                    'Add latest record',
                    style:
                        TextStyle(fontSize: 28.0, fontWeight: FontWeight.w800),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    'Blood Glucose (mmol/L)',
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: userDataBG.bg.toString(),
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Your blood glucose(mmol/L)'),
                    keyboardType: TextInputType.number,
                    // ignore: missing_return
                    validator: (val) {
                      if (val.length == 0 ||
                          double.parse(val) == 0.0 ||
                          double.parse(val) < 1.0 ||
                          double.parse(val) > 40.0) {
                        return ('Blood glucose should between 1.0 to 40.0');
                      }
                    },
                    onChanged: (val) =>
                        setState(() => _currentbg = double.parse(val)),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    'Select current status',
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  DropdownButtonFormField(
                    decoration: textInputDecoration,
                    onChanged: (value) {
                      setState(() => {
                            _currentMealStatus = value,
                            print(_currentMealStatus)
                          });
                    },
                    items: mealstatus.map((status) {
                      return DropdownMenuItem(
                        value: status,
                        child: Text(
                          '$status',
                          style: TextStyle(fontSize: 22),
                        ),
                      );
                    }).toList(),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  RaisedButton(
                      color: Colors.lightBlueAccent,
                      child: Text(
                        'Add',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          if (_currentbg == null) _currentbg = _initcurrentbg;
                          if (_currentMealStatus == null)
                            _currentMealStatus = _initMealStatus;

                          _currentBgTag =
                              Calculation.bgCal(_currentMealStatus, _currentbg);
                          DocumentReference tagCollection = Firestore.instance
                              .collection('Tag')
                              .document(user.uid);
                          DocumentSnapshot tagList = await tagCollection.get();
                          List Tags = tagList.data['Tags'];

                          if (Tags.contains('N/A') ||
                              Tags.contains('Normal Glucose Level') ||
                              Tags.contains('High Blood Sugar') ||
                              Tags.contains('Low Blood Sugar')) {
                            if (Tags.contains('N/A'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['N/A'])
                              });
                            if (Tags.contains('Normal Glucose Level'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['Normal Glucose Level'])
                              });
                            if (Tags.contains('High Blood Sugar'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['High Blood Sugar'])
                              });
                            if (Tags.contains('Low Blood Sugar'))
                              tagCollection.updateData({
                                'Tags': FieldValue.arrayRemove(['Low Blood Sugar'])
                              });
                            tagCollection.updateData({
                              'Tags': FieldValue.arrayUnion([_currentBgTag])
                            });
                          } else {
                            tagCollection.updateData({
                              'Tags': FieldValue.arrayUnion([_currentBgTag])
                            });
                          }

                          await DatabaseService(uid: user.uid).updateBG(
                            _currentBgTag ?? _initcurrentBgTag,
                            double.parse(_currentbg.toStringAsFixed(1)) ??
                                _initcurrentbg,
                            _currentMealStatus ?? _initMealStatus,
                          );
                          await DatabaseService(uid: user.uid).updateBGList(
                              _currentBgTag,
                              double.parse(_currentbg.toStringAsFixed(1)),
                              _currentMealStatus,
                              Timestamp.now());

                          Navigator.pop(context);
                        } else {
                          print('value error ! cannot save!');
                        }
                      }),
                  SizedBox(
                    height: 250,
                  ),
                  Text(
                    "Swipe down to close",
                    style: TextStyle(fontSize: 20, color: Colors.black45),
                  )
                ],
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}
