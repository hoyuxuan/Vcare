import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/constants.dart';
import 'package:provider/provider.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/models/Calculation.dart';

class EditBPForm extends StatefulWidget {
  @override
  _EditBPFormState createState() => _EditBPFormState();
}

class _EditBPFormState extends State<EditBPForm> {
  final _formKey = GlobalKey<FormState>();

//form value
  num _currentSYS;
  num _currentDIA;
  num _currentPUL;
  String _currentBPTag;
  num _initcurrentSYS;
  num _initcurrentDIA;
  num _initcurrentPUL;
  String _initcurrentBPTag;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDataBP>(
        stream: DatabaseService(uid: user.uid).userBP,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserDataBP userDataBP = snapshot.data;
            _initcurrentSYS = userDataBP.sys;
            _initcurrentDIA = userDataBP.dia;
            _initcurrentPUL = userDataBP.pul;
            return Scaffold(
              resizeToAvoidBottomPadding: false,
              resizeToAvoidBottomInset: true,
              body: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Text(
                        'Add latest record',
                        style: TextStyle(
                          fontSize: 28.0,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        'Systolic Blood Pressure(SBP)',
                        style: TextStyle(fontSize: 22),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        style: TextStyle(fontSize: 22),
                        initialValue: userDataBP.sys.toString(),
                        decoration: textInputDecoration.copyWith(
                            hintText: 'SYS Value '),
                        keyboardType: TextInputType.number,
                        // ignore: missing_return
                        validator: (val) {
                          if (val.length == 0 ||
                              double.parse(val) == 0.0 ||
                              double.parse(val) < 70.0) {
                            return ('Error , SYS value should more than 70mmHg');
                          }
                        },
                        onChanged: (val) =>
                            setState(() => _currentSYS = int.parse(val)),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        'Diastolic Blood Pressure (DBP)',
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        style: TextStyle(fontSize: 22),
                        initialValue: userDataBP.dia.toString(),
                        keyboardType: TextInputType.number,
                        decoration: textInputDecoration.copyWith(
                            hintText: 'DIA value '),
                        // ignore: missing_return
                        validator: (value) {
                          if (value.length == 0 ||
                              double.parse(value) == 0.0 ||
                              double.parse(value) < 40.0) {
                            return ('Error , DIA value should more than 40mmHg');
                          }
                        },
                        onChanged: (val) =>
                            setState(() => _currentDIA = int.parse(val)),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        'Heart Rate (bpm)',
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        style: TextStyle(fontSize: 22),
                        initialValue: userDataBP.pul.toString(),
                        decoration: textInputDecoration.copyWith(
                            hintText: 'your heart rate/ minute'),
                        keyboardType: TextInputType.number,
                        // ignore: missing_return
                        validator: (val) {
                          if (val.length == 0 ||
                              double.parse(val) == 0.0 ||
                              double.parse(val) < 40.0) {
                            return ('Error , your pulse value should more than 40pulse/minute');
                          }
                        },
                        onChanged: (val) =>
                            setState(() => _currentPUL = int.parse(val)),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      RaisedButton(
                          color: Colors.lightBlueAccent,
                          child: Text(
                            'Update',
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              if (_currentSYS == null)
                                _currentSYS = _initcurrentSYS;
                              if (_currentDIA == null)
                                _currentDIA = _initcurrentDIA;
                              if (_currentPUL == null)
                                _currentPUL = _initcurrentPUL;

                              _currentBPTag =
                                  Calculation.bpCal(_currentSYS, _currentDIA);
                              DocumentReference tagCollection = Firestore
                                  .instance
                                  .collection('Tag')
                                  .document(user.uid);
                              DocumentSnapshot tagList =
                                  await tagCollection.get();
                              List Tags = tagList.data['Tags'];
                              if (Tags.contains('N/A') ||
                                  Tags.contains('Low BP') ||
                                  Tags.contains('Normal BP') ||
                                  Tags.contains('HBP')) {
                                if (Tags.contains('N/A'))
                                  tagCollection.updateData({
                                    'Tags': FieldValue.arrayRemove(['N/A'])
                                  });
                                if (Tags.contains('Low BP'))
                                  tagCollection.updateData({
                                    'Tags': FieldValue.arrayRemove(['Low BP'])
                                  });
                                if (Tags.contains('Normal BP'))
                                  tagCollection.updateData({
                                    'Tags':
                                        FieldValue.arrayRemove(['Normal BP'])
                                  });
                                if (Tags.contains('HBP'))
                                  tagCollection.updateData({
                                    'Tags': FieldValue.arrayRemove(['HBP'])
                                  });
                                if (Tags.contains('Abnormal Value'))
                                  tagCollection.updateData({
                                    'Tags': FieldValue.arrayRemove(
                                        ['Abnormal Value'])
                                  });
                                if (_currentBPTag == "High-normal BP" ||
                                    _currentBPTag == "High BP - Severe" ||
                                    _currentBPTag ==
                                        "Isolated Systolic Hypertension") {
                                  tagCollection.updateData({
                                    'Tags': FieldValue.arrayUnion(["HBP"])
                                  });
                                } else {
                                  tagCollection.updateData({
                                    'Tags':
                                        FieldValue.arrayUnion([_currentBPTag])
                                  });
                                }
                              } else {
                                if (_currentBPTag == "High-normal BP" ||
                                    _currentBPTag == "High BP - Severe" ||
                                    _currentBPTag ==
                                        "Isolated Systolic Hypertension") {
                                  tagCollection.updateData({
                                    'Tags': FieldValue.arrayUnion(["HBP"])
                                  });
                                } else {
                                  tagCollection.updateData({
                                    'Tags':
                                        FieldValue.arrayUnion([_currentBPTag])
                                  });
                                }
                              }
                              await DatabaseService(uid: user.uid).updateBP(
                                _currentBPTag ?? _initcurrentBPTag,
                                _currentSYS ?? _initcurrentSYS,
                                _currentDIA ?? _initcurrentDIA,
                                _currentPUL ?? _initcurrentPUL,
                              );
                              await DatabaseService(uid: user.uid).updateBPList(
                                  _currentBPTag,
                                  int.parse(_currentSYS.toStringAsFixed(0))??_initcurrentSYS,
                                  int.parse(_currentDIA.toStringAsFixed(0))?? _initcurrentDIA,
                            int.parse(_currentPUL.toStringAsFixed(0))?? _initcurrentPUL,
                                  Timestamp.now());
                              Navigator.pop(context);
                            } else {
                              print('value error ! cannot save!');
                            }
                          }),
                      SizedBox(
                        height: 300,
                      ),
                      Text(
                        "Swipe down to close",
                        style: TextStyle(fontSize: 20, color: Colors.black45),
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}
