import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/loading.dart';

class EditAllergyCheckbox extends StatefulWidget {
  @override
  _EditAllergyCheckboxState createState() => _EditAllergyCheckboxState();
}

class _EditAllergyCheckboxState extends State<EditAllergyCheckbox> {

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserAllergy>(
      stream: DatabaseService(uid: user.uid).userAllergy,
      builder: (context, snapshot) {
        if(snapshot.hasData){
          UserAllergy userAllergy = snapshot.data;
          Map<String,bool>values = {
            'Dairy':userAllergy.Dairy,
            'Egg':userAllergy.Egg,
            'Gluten':userAllergy.Gluten,
            'Grain':userAllergy.Grain,
            'Peanut':userAllergy.Peanut,
            'Seafood':userAllergy.Seafood,
            'Sesame':userAllergy.Sesame,
            'Shellfish':userAllergy.Shellfish,
            'Soy':userAllergy.Soy,
            'Sulfite':userAllergy.Sulfite,
            'Tree Nut':userAllergy.Tree_Nut,
            'Wheat':userAllergy.Wheat,

          };
          DocumentReference AllergyCollection = Firestore.instance
          .collection('UserData').document(user.uid)
          .collection('med_report').document('Allergies');

          setCheckbox() async {
            values.forEach((key, value) {
              if (value == true) {

                AllergyCollection.updateData({"$key": true});
              }
              if (value == false) {

                AllergyCollection.updateData({"$key": false});
              }
            });
          }

          return Column(
            children: [
              Expanded(
                child: ListView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  children: values.keys.map((String key) {
                    return new CheckboxListTile(
                      title: Text(key,style: TextStyle(fontSize: 25,fontWeight: FontWeight.w500),),
                      value: values[key],
                      onChanged: (bool value) {
                        setState(() => {values[key] = value, setCheckbox()});
                      },
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                color: Colors.lightBlue,
                child: Text(
                  'Update',
                  style: TextStyle(fontSize: 20,color: Colors.white),
                ),
                onPressed: ()async{
                  Navigator.pop(context);
                },
              )
            ],
          );

        }
        return Loading();
      }
    );
  }
}
