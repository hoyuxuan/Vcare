import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:vcare/models/user.dart';
import 'package:intl/intl.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/constants.dart';

class BGList extends StatefulWidget {
  @override
  _BGListState createState() => _BGListState();
}

class _BGListState extends State<BGList> {
  var isSelected = false;
  var mycolor = Colors.white;

  navigateToDetail(DocumentSnapshot post) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailPage(
                  post: post,
                )));
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    Future getPost() async {
      var fs = Firestore.instance;
      Query q = fs
          .collection('UserData')
          .document(user.uid)
          .collection('med_report')
          .document('UserRecords')
          .collection('BG_List')
          .orderBy('dateTime', descending: true);
      QuerySnapshot qn = await q.getDocuments();
      return qn.documents;
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[900],
          title: Text(
            "Past Blood Glucose Records",
            style: TextStyle(color: Colors.white, fontSize: 25),
          ),
        ),
        body: Container(
            child: FutureBuilder(
                future: getPost(),
                builder: (_, snapshot) {
                  return ListView.builder(
                      itemCount: snapshot.data.length - 1,
                      itemBuilder: (_, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListTile(
                            onTap: () =>
                                navigateToDetail(snapshot.data[index + 1]),
                            title: Text(
                              snapshot.data[index + 1].data['bg'].toString() +
                                  "mmol/L    ",
                              style: TextStyle(fontSize: 25),
                            ),
                            subtitle: Text(
                              DateFormat.MMMd().add_jm().format(snapshot
                                  .data[index + 1].data['dateTime']
                                  .toDate()),
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        );
                      });
                })));
  }
}

class showAlertDialog extends StatefulWidget {
  final Data data;

  showAlertDialog({this.data});

  @override
  _showAlertDialogState createState() => _showAlertDialogState();
}

class _showAlertDialogState extends State<showAlertDialog> {
  Timestamp dateTime;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    CollectionReference BGlistRef = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BG_List');

    dateTime = widget.data.dateTime;

    Future deleteBGRecord(context) async {
      return await BGlistRef.document(dateTime.toString()).delete();
    }

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Delete"),
      onPressed: () {
        deleteBGRecord(context);
        Navigator.of(context, rootNavigator: true).pop();
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 3;
        });
      },
    );
    SchedulerBinding.instance.addPostFrameCallback((_) {
      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        content: Text("Delete this record?"),
        actions: [
          cancelButton,
          continueButton,
        ],
      );
      // show the dialog

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    });
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white24,
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  final DocumentSnapshot post;

  DetailPage({this.post});

  @override
  _DetailPageState createState() => _DetailPageState();
}

class Data {
  num bg;
  String tag;
  String mealStatus;
  Timestamp dateTime;

  Data({this.bg, this.tag, this.mealStatus, this.dateTime});
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    final data = Data(
      bg: widget.post.data['bg'],
      tag: widget.post.data['tag'],
      mealStatus: widget.post.data['mealStatus'],
      dateTime: widget.post['dateTime'],
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        elevation: 0.0,
        actions: <Widget>[
          Row(
            children: [
              FlatButton.icon(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EditBGForm2(
                                data: data,
                              )));
                  // _editBMIPanel();
                },
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                label: Text(
                  'Edit',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              FlatButton.icon(
                onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => showAlertDialog(
                                data: data,
                              )))
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
                label: Text(
                  'Delete',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(children: [
          SizedBox(
            height: 30,
          ),
          Text(
            DateFormat.yMMMMd('en_US')
                .add_jm()
                .format(widget.post.data['dateTime'].toDate()),
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 50,
          ),
          Text(
            widget.post.data['bgTag'],
            style: TextStyle(fontSize: 50, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 40),
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(widget.post.data['bg'].toString(),
                          style: TextStyle(fontSize: 30)),
                      Text(" mmol/L", style: TextStyle(fontSize: 20))
                    ],
                  ),
                ),
                VerticalDivider(color: Colors.black26,thickness: 2.0,),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Text("Record status:", style: TextStyle(fontSize: 18)),
                      SizedBox(height: 10,),
                      Text(widget.post.data['mealtime'].toString(),
                          style: TextStyle(fontSize: 30)),

                    ],
                  ),
                ),

              ],
            ),
          ),
          SizedBox(
            height: 25,
          ),
        ]),
      ),
    );
  }
}

class EditBGForm2 extends StatefulWidget {
  final Data data;

  EditBGForm2({this.data});

  @override
  _EditBGForm2State createState() => _EditBGForm2State();
}

class _EditBGForm2State extends State<EditBGForm2> {
  final _formKey = GlobalKey<FormState>();
  final List<String> mealstatus = ['Fasting', 'Non-Fasting'];

  double _currentbg;
  String _currentBgTag;
  String _currentMealStatus;
  double _initcurrentbg;
  String _initMealStatus;
  Timestamp _initrecordTimestamp;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    _initcurrentbg = widget.data.bg;
    _initMealStatus = widget.data.mealStatus;
    _initrecordTimestamp = widget.data.dateTime;

    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Edit record",
            style: TextStyle(fontSize: 25),
          ),
          backgroundColor: Colors.blue[900],
          centerTitle: true,
        ),
        body: Container(
          padding: EdgeInsets.all(22.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 30.0,
                ),
                Text(
                  'Your Blood Glucose (mmol/L)',
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFormField(
                  style: TextStyle(fontSize: 22),
                  initialValue: _initcurrentbg.toStringAsFixed(2),
                  decoration: textInputDecoration.copyWith(
                      hintText: 'Enter your blood glucose'),
                  keyboardType: TextInputType.number,
                  // ignore: missing_return
                  validator: (val) {
                    if (val.length == 0 ||
                        double.parse(val) == 0.0 ||
                        double.parse(val) < 1.0 ||
                        double.parse(val) > 40.0) {
                      return ('Blood glucose should between 1.0 to 40.0');
                    }
                  },
                  onChanged: (val) =>
                      setState(() => _currentbg = double.parse(val)),
                ),
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  'Current status',
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(
                  height: 10.0,
                ),
                DropdownButtonFormField(
                  value: _initMealStatus,
                  decoration: textInputDecoration,
                  onChanged: (value) {
                    setState(() => {
                          _currentMealStatus = value,
                          print(_currentMealStatus)
                        });
                  },
                  items: mealstatus.map((status) {
                    return DropdownMenuItem(
                      value: status,
                      child: Text(
                        '$status',
                        style: TextStyle(fontSize: 22),
                      ),
                    );
                  }).toList(),
                ),
                SizedBox(
                  height: 25.0,
                ),
                RaisedButton(
                    color: Colors.lightBlueAccent,
                    child: Text(
                      'Update',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        if (_currentbg == null) _currentbg = _initcurrentbg;
                        if (_currentMealStatus == null)
                          _currentMealStatus = _initMealStatus;

                        await DatabaseService(uid: user.uid).updateBGList(
                            _currentBgTag,
                            double.parse(_currentbg.toStringAsFixed(1)) ??
                                _initcurrentbg,
                            _currentMealStatus,
                            _initrecordTimestamp);
                        Navigator.popUntil(
                            context, ModalRoute.withName('/viewBGTrend'));
                      } else {
                        print('value error ! cannot save!');
                      }
                    }),
              ],
            ),
          ),
        ));
  }
}
