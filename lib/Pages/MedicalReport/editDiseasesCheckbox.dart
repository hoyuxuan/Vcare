import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/loading.dart';

class EditDiseasesCheckbox extends StatefulWidget {
  @override
  _EditDiseasesCheckboxState createState() => _EditDiseasesCheckboxState();
}

class _EditDiseasesCheckboxState extends State<EditDiseasesCheckbox> {


  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserDisease>(
        stream: DatabaseService(uid: user.uid).userDisease,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserDisease userDisease = snapshot.data;
            Map<String, bool> values = {
              'Alzheimer': userDisease.alzheimer,
              'Arthritis': userDisease.arthritis,
              'Asthma': userDisease.asthma,
              'Cancer': userDisease.cancer,
              'Diabetes': userDisease.diabetes,
              'High Blood Pressure (Hypertension)': userDisease.hbp,
              'High Cholesterol': userDisease.hc,
              'Heart Disease': userDisease.heartDisease,
              'Obesity': userDisease.obesity,
              'Osteoporosis': userDisease.osteoporosis,
            };

            DocumentReference DiseaseCollection = Firestore.instance
                .collection('UserData')
                .document(user.uid)
                .collection('med_report')
                .document('Diseases');

            setCheckbox() async {
              values.forEach((key, value) {
                if (value == true) {

                  DiseaseCollection.updateData({"$key": true});
                }
                if (value == false) {

                  DiseaseCollection.updateData({"$key": false});
                }
              });
            }

            return Column(
              children: [
                Expanded(
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    children: values.keys.map((String key) {
                      return new CheckboxListTile(
                        title: Text(key,style: TextStyle(fontSize: 25,fontWeight: FontWeight.w500),),
                        value: values[key],
                        onChanged: (bool value) {
                          setState(() => {values[key] = value, setCheckbox()});
                        },
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RaisedButton(
                  color: Colors.lightBlue,
                  child: Text(
                    'Update',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                  },
                )
              ],
            );
          } else
            return Loading();
        });
  }
}
