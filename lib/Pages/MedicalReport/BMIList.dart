import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:vcare/models/Calculation.dart';
import 'package:vcare/models/user.dart';
import 'package:intl/intl.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/shared/constants.dart';

class BMIList extends StatefulWidget {
  @override
  _BMIListState createState() => _BMIListState();
}

class _BMIListState extends State<BMIList> {
  navigateToDetail(DocumentSnapshot post) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailPage(
                  post: post,
                )));
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    Future getPost() async {
      var fs = Firestore.instance;
      Query q = fs
          .collection('UserData')
          .document(user.uid)
          .collection('med_report')
          .document('UserRecords')
          .collection('BMI_List')
          .orderBy("dateTime",descending: true);
      QuerySnapshot qn = await q.getDocuments();
      return qn.documents;
    }


    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[900],
          title: Text(
            "Past BMI Records",
            style: TextStyle(color: Colors.white, fontSize: 25),
          ),
        ),
        body: Container(
            child: FutureBuilder(
                future: getPost(),
                builder: (_, snapshot) {
                  return ListView.builder(
                      itemCount: snapshot.data.length-1,
                      itemBuilder: (_, index) {

                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListTile(
                            onTap: () => navigateToDetail(snapshot.data[index+1]),
                            title: Text(
                              snapshot.data[index+1].data['height'].toString() +
                                  "cm    " +
                                  snapshot.data[index+1].data['weight']
                                      .toString() +
                                  "kg",
                              style: TextStyle(fontSize: 25),
                            ),
                            subtitle: Text(
                              DateFormat.MMMd().add_jm().format(snapshot
                                  .data[index+1].data['dateTime']
                                  .toDate()),
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        );
                      });
                })));
  }
}

class showAlertDialog extends StatefulWidget {
  final Data data;

  showAlertDialog({this.data});

  @override
  _showAlertDialogState createState() => _showAlertDialogState();
}

class _showAlertDialogState extends State<showAlertDialog> {
  Timestamp dateTime;

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    CollectionReference BMIlistRef = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BMI_List');

    dateTime = widget.data.dateTime;

    Future deleteBMIRecord(context) async {
      return await BMIlistRef.document(dateTime.toString()).delete();
    }


      // set up the buttons
      Widget cancelButton = FlatButton(
        child: Text("Cancel"),
        onPressed: () {
         int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 2;
          });
        },
      );
      Widget continueButton = FlatButton(
        child: Text("Delete"),
        onPressed: () {
          deleteBMIRecord(context);
          Navigator.of(context, rootNavigator: true).pop();
          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 3;
          });
        },
      );
    SchedulerBinding.instance.addPostFrameCallback((_) {
      // set up the AlertDialog
      AlertDialog alert = AlertDialog(

        content: Text("Delete this record?"),
        actions: [
          cancelButton,
          continueButton,
        ],
      );
      // show the dialog

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    });
  return Container(decoration: new BoxDecoration(
    color: Colors.white24,
  ),);}
}

class DetailPage extends StatefulWidget {
  final DocumentSnapshot post;

  DetailPage({this.post});

  @override
  _DetailPageState createState() => _DetailPageState();
}

class Data {
  num height;
  num weight;
  String tag;
  Timestamp dateTime;

  Data({this.height, this.weight, this.tag, this.dateTime});
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {

    final data = Data(
      height: widget.post.data['height'],
      weight: widget.post.data['weight'],
      tag: widget.post.data['bmiTag'],
      dateTime: widget.post.data['dateTime'],
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        elevation: 0.0,
        actions: <Widget>[
          Row(
            children: [
              FlatButton.icon(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EditBMIForm2(
                                data: data,
                              )));
                  // _editBMIPanel();
                },
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                label: Text(
                  'Edit',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              FlatButton.icon(
                onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => showAlertDialog(
                                data: data,
                              )))
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
                label: Text(
                  'Delete',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          )
        ],
      ),
      body: Column(children: [
        SizedBox(
          height: 30,
        ),
        Text(
          DateFormat.yMMMMd('en_US')
              .add_jm()
              .format(widget.post.data['dateTime'].toDate()),
          style: TextStyle(fontSize: 20),
        ),
        SizedBox(
          height: 50,
        ),
        Text(
          widget.post.data['bmiTag'],
          style: TextStyle(fontSize: 50, fontWeight: FontWeight.w700),
        ),
        SizedBox(height: 40),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(widget.post.data['height'].toString(),
                      style: TextStyle(fontSize: 30)),
                  Text(" CM", style: TextStyle(fontSize: 20))
                ],
              ),
            ),
            SizedBox(width: 20),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(widget.post.data['weight'].toString(),
                      style: TextStyle(fontSize: 30)),
                  Text(" KG", style: TextStyle(fontSize: 20))
                ],
              ),
            ),
          ],
        ),
        SizedBox(
          height: 25,
        ),
      ]),
    );
  }
}

class EditBMIForm2 extends StatefulWidget {
  final Data data;

  EditBMIForm2({this.data});

  @override
  _EditBMIForm2State createState() => _EditBMIForm2State();
}

class _EditBMIForm2State extends State<EditBMIForm2> {
  final _formKey = GlobalKey<FormState>();

  double _currentHeight;
  double _currentWeight;
  String _currentBmiTag;
  double _initcurrentHeight;
  double _initcurrentWeight;
  Timestamp _initrecordTimestamp;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    _initcurrentHeight = widget.data.height;
    _initcurrentWeight = widget.data.weight;
    _initrecordTimestamp = widget.data.dateTime;

    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Edit record",
            style: TextStyle(fontSize: 25),
          ),
          backgroundColor: Colors.blue[900],
          centerTitle: true,
        ),
        body: Container(
          padding: EdgeInsets.all(22.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 30.0,
                ),
                Text(
                  'Your Height (cm)',
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFormField(
                  style: TextStyle(fontSize: 22),
                  initialValue: _initcurrentHeight.toStringAsFixed(2),
                  decoration: textInputDecoration.copyWith(
                      hintText: 'Enter your Height'),
                  keyboardType: TextInputType.number,
                  // ignore: missing_return
                  validator: (val) {
                    if (val.length == 0 ||
                        double.parse(val) == 0.0 ||
                        double.parse(val) < 100.0) {
                      return ('Height should be more then 100.0 or is a Number');
                    }
                  },
                  onChanged: (val) =>
                      setState(() => _currentHeight = double.parse(val)),
                ),
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  'Your Weight (kg)',
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFormField(
                  style: TextStyle(fontSize: 22),
                  initialValue: _initcurrentWeight.toStringAsFixed(1),
                  keyboardType: TextInputType.number,
                  decoration: textInputDecoration.copyWith(
                      hintText: 'Enter your Weight'),
                  // ignore: missing_return
                  validator: (value) {
                    if (value.length == 0 ||
                        double.parse(value) == 0.0 ||
                        double.parse(value) < 20.0) {
                      return ('Weight should be more then 20.0 or is a Number');
                    }
                  },
                  onChanged: (val) =>
                      setState(() => _currentWeight = double.parse(val)),
                ),
                SizedBox(
                  height: 25.0,
                ),
                RaisedButton(
                    color: Colors.lightBlueAccent,
                    child: Text(
                      'Update',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        if (_currentWeight == null)
                          _currentWeight = _initcurrentWeight;
                        if (_currentHeight == null)
                          _currentHeight = _initcurrentHeight;

                        _currentBmiTag =
                            Calculation.bmiCal(_currentHeight, _currentWeight);
                        DocumentReference tagCollection = Firestore.instance
                            .collection('Tag')
                            .document(user.uid);
                        DocumentSnapshot tagList = await tagCollection.get();
                        List Tags = tagList.data['Tags'];
                        if (Tags.contains('N/A') ||
                            Tags.contains('Overweight') ||
                            Tags.contains('Underweight') ||
                            Tags.contains('Normal Weight')) {
                          if (Tags.contains('N/A'))
                            tagCollection.updateData({
                              'Tags': FieldValue.arrayRemove(['N/A'])
                            });
                          if (Tags.contains('Overweight'))
                            tagCollection.updateData({
                              'Tags': FieldValue.arrayRemove(['Overweight'])
                            });
                          if (Tags.contains('Normal Weight'))
                            tagCollection.updateData({
                              'Tags': FieldValue.arrayRemove(['Normal Weight'])
                            });
                          if (Tags.contains('Underweight'))
                            tagCollection.updateData({
                              'Tags': FieldValue.arrayRemove(['Underweight'])
                            });
                          tagCollection.updateData({
                            'Tags': FieldValue.arrayUnion([_currentBmiTag])
                          });
                        } else {
                          tagCollection.updateData({
                            'Tags': FieldValue.arrayUnion([_currentBmiTag])
                          });
                        }

                        await DatabaseService(uid: user.uid).updateBMIList(
                            _currentBmiTag,
                            _currentHeight,
                            _currentWeight,
                            _initrecordTimestamp);

                        Navigator.popUntil(
                            context, ModalRoute.withName('/viewBMITrend'));
                      } else {
                        print('value error ! cannot save!');
                      }
                    }),
              ],
            ),
          ),
        ));
  }
}
