import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:vcare/Pages/MedicalReport/editBPform.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/models/user.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import 'BPList.dart';

class viewBPTrend extends StatefulWidget {
  @override
  _viewBPTrendState createState() => _viewBPTrendState();
}

class _viewBPTrendState extends State<viewBPTrend> {

  createAlertDialog(BuildContext context) {
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text("Blood Pressure Tips"),
        content: Text(
            "hgfdjkghdfghdfgjdfgdf g dfgffdgdfgdf g g fhfgfhgfhgfhfghgf hgfhfghgfhfdhdfhdf hfghgfhgfhfh fhfhfhdfgh"),
        actions: <Widget>[MaterialButton(
          child: Text('Close'),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        ],
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    void _showEditBPPanel() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.fromLTRB(40,60,40,5),
              child: EditBPForm(),
            );
          },
          isScrollControlled: true);
    }

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          centerTitle: true,
          title: Text('Blood Pressure',
              style: TextStyle(
                fontSize: 25,
              )),
          backgroundColor: Colors.lightBlue[900],
          bottom: TabBar(
            tabs: [
              Tab(
                  icon: Icon(Icons.article_outlined),
                  child: Text(
                    "Track",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
              Tab(
                  icon: Icon(Icons.stacked_line_chart),
                  child: Text(
                    "Trends",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _showEditBPPanel(),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)),
          label: Text('Record manually', style: TextStyle(fontSize: 22),),
          // child: Container(
          //   constraints: BoxConstraints(
          //       maxWidth: 300.0, minHeight: 50.0),
          //   alignment: Alignment.center,
          //   child: Text(
          //     "Record manually",
          //     textAlign: TextAlign.center,
          //     style: TextStyle(
          //         fontSize: 22, color: Colors.white),
          //   ),
          // ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: TabBarView(
          children: [
            StreamBuilder<UserDataBP>(
              stream: DatabaseService(uid: user.uid).userBP,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  UserDataBP BP = snapshot.data;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                          child: Column(
                            children: [
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Column(children: [
                                    Text('Your Blood Pressure Result',
                                        style: TextStyle(fontSize: 28)),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        child: Icon(
                                            Icons.info_outline
                                        ),
                                        onTap: () {
                                          createAlertDialog(context);
                                        },
                                      ),
                                    ),
                                    Text(
                                      BP.hbpTag.toString(),
                                      style: TextStyle(
                                          fontSize: 50,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(height: 40),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(
                                                  BP.sys.toString() +
                                                      " / " +
                                                      BP.dia.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(' mmHg',
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(BP.pul.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(" bpm",
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 25,),

                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(builder: (
                                                BuildContext context) =>
                                                BPList()));
                                      },
                                      child: Container(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            children: [Icon(Icons.history),
                                              Text(" Past Records",
                                                style: TextStyle(
                                                    fontSize: 20),),
                                            ],
                                          )),
                                    ),

                                  ]),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Loading();
                }
              },
            ),
            Container(child: BPchart()),
          ],
        ),
      ),
    );
  }
}

class BPchart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    final CollectionReference fireData = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BP_List');
    return StreamBuilder(
      stream: fireData.snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        Widget widget;
        if (snapshot.hasData) {
          List<ChartData> chartData = <ChartData>[];
          for (int index = 0; index < snapshot.data.documents.length; index++) {
            DocumentSnapshot documentSnapshot = snapshot.data.documents[index];

            // here we are storing the data into a list which is used for chart’s data source
            chartData.add(ChartData.fromMap(documentSnapshot.data));
          }
          return Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15.0, 10, 15, 75),
                child: SfCartesianChart(
                  trackballBehavior: TrackballBehavior(
                    // Enables the trackball
                    enable: true,
                    tooltipSettings: InteractiveTooltip(
                        enable: true,
                        color: Colors.red
                    ),),
                  legend: Legend(
                      isVisible: true,
                      toggleSeriesVisibility: true
                  ),
                  title: ChartTitle(
                      text: 'Blood Pressure trend',
                      // Aligns the chart title to left
                      alignment: ChartAlignment.center,
                      textStyle: TextStyle(
                        color: Colors.blueAccent,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                      )),
                  zoomPanBehavior: ZoomPanBehavior(
                      enableDoubleTapZooming: true,
                      enablePinching: true,
                      enablePanning: true,
                      enableMouseWheelZooming: true,
                      enableSelectionZooming: false,
                      zoomMode: ZoomMode.x,
                      maximumZoomLevel: 2
                  ),
                  primaryXAxis: DateTimeAxis(
                    plotBands: <PlotBand>[
                      PlotBand(
                          verticalTextPadding: '5%',
                          horizontalTextPadding: '5%',
                          textAngle: 0,
                          text: 'Average',
                          start: 100,
                          end: 120,
                          textStyle:
                          TextStyle(color: Colors.deepOrange, fontSize: 16),
                          borderColor: Colors.red,
                          borderWidth: 2)
                    ],
                    // interval: 1,
                    // intervalType: DateTimeIntervalType.auto,
                  ),
                  series: <ChartSeries<ChartData, dynamic>>[
                    LineSeries<ChartData, dynamic>(
                        name: 'DIA (mmHg)',
                        dataSource: chartData,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        ),
                        xValueMapper: (ChartData data, _) => data.xValue,
                        yValueMapper: (ChartData data, _) => data.yValue),
                    LineSeries<ChartData, dynamic>(
                        name: 'SYS (mmHg)',
                        dataSource: chartData,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        ),
                        xValueMapper: (ChartData data, _) => data.xValue,
                        yValueMapper: (ChartData data, _) => data.y2Value),
                    LineSeries<ChartData, dynamic>(
                        name: 'PUL (bpm)',
                        dataSource: chartData,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        ),
                        xValueMapper: (ChartData data, _) => data.xValue,
                        yValueMapper: (ChartData data, _) => data.y3Value),
                  ],
                ),
              ));
        }
        return widget;
      },
    );
  }
}

class ChartData {
  ChartData({this.xValue, this.yValue, this.y2Value, this.y3Value});

  ChartData.fromMap(Map<String, dynamic> dataMap)
      : xValue = dataMap['dateTime'],
        yValue = dataMap['dia'],
        y2Value = dataMap['sys'],
        y3Value = dataMap['pul'];
  final Timestamp xValue;
  final int yValue;
  final int y2Value;
  final int y3Value;
}

