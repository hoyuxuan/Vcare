import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/models/user.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'BGList.dart';
import 'editBGform.dart';

class viewBGTrend extends StatefulWidget {
  @override
  _viewBGTrendState createState() => _viewBGTrendState();
}

class _viewBGTrendState extends State<viewBGTrend> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    void _showEditBGPanel() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 60.0, horizontal: 40.0),
              child: Column(
                children: [
                  EditBGForm(),
                ],
              ),
            );
          },
          isScrollControlled: true);
    }

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          centerTitle: true,
          title: Text('Blood Glucose',
              style: TextStyle(
                fontSize: 25,
              )),
          backgroundColor: Colors.blue[900],
          bottom: TabBar(
            tabs: [
              Tab(
                  icon: Icon(Icons.article_outlined),
                  child: Text(
                    "Track",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
              Tab(
                  icon: Icon(Icons.stacked_line_chart),
                  child: Text(
                    "Trends",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _showEditBGPanel(),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          label: Text(
            'Record manually',
            style: TextStyle(fontSize: 22),
          ),
          // child: Container(
          //   constraints: BoxConstraints(
          //       maxWidth: 300.0, minHeight: 50.0),
          //   alignment: Alignment.center,
          //   child: Text(
          //     "Record manually",
          //     textAlign: TextAlign.center,
          //     style: TextStyle(
          //         fontSize: 22, color: Colors.white),
          //   ),
          // ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: TabBarView(
          children: [
            SingleChildScrollView(
              child: StreamBuilder<UserDataBG>(
                stream: DatabaseService(uid: user.uid).userBG,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    UserDataBG BG = snapshot.data;
                    return Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Column(children: [
                                    Text('Your Blood Glucose Result',
                                        style: TextStyle(fontSize: 28)),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        child: Icon(
                                          Icons.info_outline
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      BG.bgTag.toString(),
                                      style: TextStyle(
                                          fontSize: 50,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(height: 40),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(BG.bg.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(" mmol/L",
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text(BG.mealtime,
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                    SizedBox(height: 25,),

                                    GestureDetector(
                                      onTap: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> BGList()));
                                      },
                                      child: Container(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [Icon(Icons.history),
                                              Text(" Past Records",style: TextStyle(fontSize: 20),),
                                            ],
                                          )),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Text("Past Records"),

                        // SingleChildScrollView(child: BMIList()),
                      ],
                    );
                  } else {
                    return Loading();
                  }
                },
              ),
            ),
            Container(child: BGchart()),
          ],
        ),
      ),
    );
  }
}



class BGchart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Color> colorOrange = <Color>[];
    colorOrange.add(Colors.deepOrange[50]);
    colorOrange.add(Colors.deepOrange[200]);
    colorOrange.add(Colors.deepOrange);

    final List<Color> colorBlue = <Color>[];
    colorBlue.add(Colors.lightBlueAccent[100]);
    colorBlue.add(Colors.lightBlueAccent[200]);
    colorBlue.add(Colors.blueAccent);

    final List<double> stopsOrange = <double>[];
    stopsOrange.add(0.0);
    stopsOrange.add(0.5);
    stopsOrange.add(1.0);

    final List<double> stopsBlue = <double>[];
    stopsBlue.add(0.0);
    stopsBlue.add(0.5);
    stopsBlue.add(1.0);

    final LinearGradient gradientColorsBlue =
    LinearGradient(colors: colorBlue, stops: stopsBlue);

    final user = Provider.of<User>(context);
    final CollectionReference fireData = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BG_List');
    return StreamBuilder(
      stream: fireData.snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        Widget widget;
        if (snapshot.hasData) {
          List<BGChartData> chartData1 = <BGChartData>[];
          for (int index = 0; index < snapshot.data.documents.length; index++) {
            DocumentSnapshot documentSnapshot = snapshot.data.documents[index];

            // here we are storing the data into a list which is used for chart’s data source
            chartData1.add(BGChartData.fromMap(documentSnapshot.data));
          }

          return Center(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 55),
                child: SfCartesianChart(
                  trackballBehavior: TrackballBehavior(
                    // Enables the trackball
                    enable: true,
                    tooltipSettings:
                    InteractiveTooltip(enable: true, color: Colors.blue[900],textStyle: TextStyle(fontSize:20 )),
                  ),
                  legend: Legend(
                      textStyle: TextStyle(fontSize: 22),
                      iconHeight: 25,
                      isVisible: true,
                      // Toogles the series visibility on tapping the legend item
                      toggleSeriesVisibility: true),
                  title: ChartTitle(
                      text: 'Blood Glocuse trend',
                      // Aligns the chart title to left
                      alignment: ChartAlignment.center,
                      textStyle: TextStyle(
                        color: Colors.blue[900],
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        fontSize: 25,
                      )),
                  zoomPanBehavior: ZoomPanBehavior(
                    enableDoubleTapZooming: true,
                    enablePinching: true,
                    enablePanning: true,
                    enableMouseWheelZooming: true,
                    enableSelectionZooming: false,
                    zoomMode: ZoomMode.x,
                  ),
                  primaryXAxis: DateTimeAxis(
                      title: AxisTitle(
                          text: 'Timestamp',
                          textStyle: TextStyle(
                            color: Colors.blueAccent[900],
                            fontFamily: 'Roboto',
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ))),
                  series: <ChartSeries<BGChartData, dynamic>>[
                    AreaSeries<BGChartData, dynamic>(
                        name: 'Blood Glucose, mmol/L',
                        enableTooltip: true,
                        dataSource: chartData1,
                        xValueMapper: (BGChartData data, _) => data.xValue,
                        yValueMapper: (BGChartData data, _) => data.yValue,
                        gradient: gradientColorsBlue,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        )),

                  ],
                )),
          );
        }
        return widget;
      },
    );
  }
}

class BGChartData {
  BGChartData({this.xValue, this.yValue,});

  BGChartData.fromMap(Map<String, dynamic> dataMap)
      : xValue = dataMap['dateTime'],
        yValue = dataMap['bg'];


  final Timestamp xValue;
  final num yValue;

}

class BG {
  BG({this.dateTime, this.bg});

  final Timestamp dateTime;
  final num bg;

}
