import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:vcare/Pages/MedicalReport/editBMIform.dart';
import 'package:vcare/shared/loading.dart';
import 'package:vcare/services/database.dart';
import 'package:vcare/models/user.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'BMIList.dart';

class viewBMITrend extends StatefulWidget {
  @override
  _viewBMITrendState createState() => _viewBMITrendState();
}

class _viewBMITrendState extends State<viewBMITrend> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    void _showEditBMIPanel() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 60.0, horizontal: 60.0),
              child: Column(
                children: [
                  EditBMIForm(),
                ],
              ),
            );
          },
          isScrollControlled: true);
    }

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          centerTitle: true,
          title: Text('BMI',
              style: TextStyle(
                fontSize: 25,
              )),
          backgroundColor: Colors.blue[900],
          bottom: TabBar(
            tabs: [
              Tab(
                  icon: Icon(Icons.article_outlined),
                  child: Text(
                    "Track",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
              Tab(
                  icon: Icon(Icons.stacked_line_chart),
                  child: Text(
                    "Trends",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _showEditBMIPanel(),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          label: Text(
            'Record manually',
            style: TextStyle(fontSize: 22),
          ),
          // child: Container(
          //   constraints: BoxConstraints(
          //       maxWidth: 300.0, minHeight: 50.0),
          //   alignment: Alignment.center,
          //   child: Text(
          //     "Record manually",
          //     textAlign: TextAlign.center,
          //     style: TextStyle(
          //         fontSize: 22, color: Colors.white),
          //   ),
          // ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: TabBarView(
          children: [
            SingleChildScrollView(
              child: StreamBuilder<UserDataBMI>(
                stream: DatabaseService(uid: user.uid).userBMI,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    UserDataBMI BMI = snapshot.data;
                    return Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Column(children: [
                                    Text('Your BMI Result',
                                        style: TextStyle(fontSize: 30)),
                                    SizedBox(
                                      height: 50,
                                    ),
                                    Text(
                                      BMI.bmiTag.toString(),
                                      style: TextStyle(
                                          fontSize: 50,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(height: 40),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(BMI.height.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(" CM",
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text(BMI.weight.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(" KG",
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                    SizedBox(height: 25,),

                                    GestureDetector(
                                      onTap: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> BMIList()));
                                      },
                                      child: Container(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [Icon(Icons.history),
                                              Text(" Past Records",style: TextStyle(fontSize: 20),),
                                            ],
                                          )),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Text("Past Records"),

                        // SingleChildScrollView(child: BMIList()),
                      ],
                    );
                  } else {
                    return Loading();
                  }
                },
              ),
            ),
            Container(child: BMIchart()),
          ],
        ),
      ),
    );
  }
}



class BMIchart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Color> colorOrange = <Color>[];
    colorOrange.add(Colors.deepOrange[50]);
    colorOrange.add(Colors.deepOrange[200]);
    colorOrange.add(Colors.deepOrange);

    final List<Color> colorBlue = <Color>[];
    colorBlue.add(Colors.lightBlueAccent[100]);
    colorBlue.add(Colors.lightBlueAccent[200]);
    colorBlue.add(Colors.blueAccent);

    final List<double> stopsOrange = <double>[];
    stopsOrange.add(0.0);
    stopsOrange.add(0.5);
    stopsOrange.add(1.0);

    final List<double> stopsBlue = <double>[];
    stopsBlue.add(0.0);
    stopsBlue.add(0.5);
    stopsBlue.add(1.0);

    final LinearGradient gradientColorsOrange =
    LinearGradient(colors: colorOrange, stops: stopsOrange);

    final LinearGradient gradientColorsBlue =
    LinearGradient(colors: colorBlue, stops: stopsBlue);

    final user = Provider.of<User>(context);
    final CollectionReference fireData = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BMI_List');
    return StreamBuilder(
      stream: fireData.snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        Widget widget;
        if (snapshot.hasData) {
          List<BMIChartData> chartData1 = <BMIChartData>[];
          for (int index = 0; index < snapshot.data.documents.length; index++) {
            DocumentSnapshot documentSnapshot = snapshot.data.documents[index];

            // here we are storing the data into a list which is used for chart’s data source
            chartData1.add(BMIChartData.fromMap(documentSnapshot.data));
          }

          return Center(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 55),
                child: SfCartesianChart(
                  trackballBehavior: TrackballBehavior(
                    // Enables the trackball
                    enable: true,
                    tooltipSettings:
                    InteractiveTooltip(enable: true, color: Colors.blue[900],textStyle: TextStyle(fontSize:20 )),
                  ),
                  legend: Legend(
                    textStyle: TextStyle(fontSize: 22),
                      iconHeight: 25,
                      isVisible: true,
                      // Toogles the series visibility on tapping the legend item
                      toggleSeriesVisibility: true),
                  title: ChartTitle(
                      text: 'Weight & Height trend',
                      // Aligns the chart title to left
                      alignment: ChartAlignment.center,
                      textStyle: TextStyle(
                        color: Colors.blue[900],
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        fontSize: 25,
                      )),
                  zoomPanBehavior: ZoomPanBehavior(
                    enableDoubleTapZooming: true,
                    enablePinching: true,
                    enablePanning: true,
                    enableMouseWheelZooming: true,
                    enableSelectionZooming: false,
                    zoomMode: ZoomMode.x,
                  ),
                  primaryXAxis: DateTimeAxis(
                      title: AxisTitle(
                          text: 'Timestamp',
                          textStyle: TextStyle(
                            color: Colors.blueAccent[900],
                            fontFamily: 'Roboto',
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ))),
                  series: <ChartSeries<BMIChartData, dynamic>>[
                    AreaSeries<BMIChartData, dynamic>(
                        name: 'Height (cm)',
                        enableTooltip: true,
                        dataSource: chartData1,
                        xValueMapper: (BMIChartData data, _) => data.xValue,
                        yValueMapper: (BMIChartData data, _) => data.yValue,
                        gradient: gradientColorsBlue,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        )),
                    AreaSeries<BMIChartData, dynamic>(
                        name: 'Weight (kg)',
                        enableTooltip: true,
                        dataSource: chartData1,
                        xValueMapper: (BMIChartData data, _) => data.xValue,
                        yValueMapper: (BMIChartData data, _) => data.y2Value,
                        gradient: gradientColorsOrange,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        )),
                  ],
                )),
          );
        }
        return widget;
      },
    );
  }
}

class BMIChartData {
  BMIChartData({this.xValue, this.yValue, this.y2Value});

  BMIChartData.fromMap(Map<String, dynamic> dataMap)
      : xValue = dataMap['dateTime'],
        yValue = dataMap['height'],
        y2Value = dataMap['weight'];

  final Timestamp xValue;
  final num yValue;
  final num y2Value;
}

class BMI {
  BMI({this.dateTime, this.height, this.weight});

  final Timestamp dateTime;
  final num height;
  final num weight;
}
