import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vcare/Pages/authenticate/rounded_input_field.dart';
import 'package:vcare/Pages/authenticate/rounded_password_field.dart';
import 'package:vcare/services/auth.dart';
import 'package:vcare/shared/loading.dart';


class Register extends StatefulWidget {

  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  //text field state
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.grey[100],
      body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 20.0,horizontal: 50.0),
          child:Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(height: size.height * 0.1),
                SvgPicture.asset(
                  "assets/icons/signup.svg",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  hintText: "Your Email",
                  onChanged: (value) {
                    setState(()=>email = value);
                  },
                ),
                SizedBox(height: 20.0),
                RoundedPasswordField(
                  onChanged: (value) {
                    setState(()=>password = value);
                  },
                ),
                SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Already have an Account ? ",style: TextStyle(fontSize: 18,)),
                    GestureDetector(
                      onTap:(){
                        widget.toggleView();
                      },
                      child: Text("Sign In",
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25,color:Colors.blueAccent
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height:20),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)
                  ),
                  color: Colors.lightBlueAccent,
                  child: Text(
                    'Register',
                    style:TextStyle(color: Colors.black,fontSize: 25),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()){
                      setState(() => loading = true);
                      dynamic result = await _auth.registerWithEmailAndPassword(email, password);
                      if(result == null){
                        setState(() {
                          error = 'Email exist/ not valid ,pls enter a valid email';
                          loading = false;
                        });
                      }
                    }
                  }
                ),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color:Colors.red, fontSize: 18.0),
                ),
              ],
            ),
          )
      ),
    );
  }
}

