import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vcare/services/auth.dart';
import '../rounded_input_field.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  final AuthService _auth = AuthService();
  String email = '';
  var _formKey = GlobalKey<FormState>();
  bool loading = false;
  String message = ' ';

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Reset Password",style: TextStyle(fontSize: 24),),
      ),
      body: SingleChildScrollView(
        child: Padding(padding: EdgeInsets.only(top: 50,left:20,right: 20),
        child: Form(
          key: _formKey,
          child: Column(
            children: [SvgPicture.asset(
              "assets/icons/forgot.svg",
              height: size.height * 0.25,
            ),
              SizedBox(height: size.height * 0.03),
              Text("We will mail a link to reset your password",
            style: TextStyle(color: Colors.blue[900],fontSize: 24,fontWeight: FontWeight.w700),),
              SizedBox(height: 10,),
              RoundedInputField(
                hintText: "Your Email",
                onChanged: (value) {
                  setState(()=>email = value);
                },
              ),
              Text(message,
                style: TextStyle(color:Colors.red, fontSize: 18.0),),
              RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                  color: Colors.lightBlueAccent,
                  child: Text(
                    'Reset password',
                    style:TextStyle(color: Colors.black,fontSize: 25),
                  ),
                  onPressed: ()async{
                    if (_formKey.currentState.validate()){
                      setState(() {
                        message = 'Please check your mails';
                      });
                      _auth.sendPasswordResetEmail(email).then((value) => print("reset mail sent !"));
                      }
                    }
              ),],

          ),
        ),
        ),
        
      ),
    );
  }
}
