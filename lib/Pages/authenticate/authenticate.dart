import 'package:flutter/material.dart';
import 'package:vcare/Pages/authenticate/register.dart';
import 'package:vcare/Pages/authenticate/sign_in.dart';


class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {

  bool showSignIn = true;
  void toggleView(){
    setState(()=>showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn){
      return Register(toggleView:toggleView);
    }else{
      return SignIn(toggleView:toggleView);
    }
  }
}
