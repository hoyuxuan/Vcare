import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vcare/Pages/authenticate/authenticate.dart';
import 'package:vcare/Pages/Home/home.dart';
import 'package:vcare/models/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    //return either Home or Authenticate widget
    if (user == null){
      return Authenticate();
    }else{
      return Home();
    }
  }
}
