import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:vcare/services/auth.dart';

class Home extends StatelessWidget {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        centerTitle: true,
        title: Text('Vcare',
            style: TextStyle(
              fontSize: 25,
            )),
        backgroundColor: Colors.lightBlue[900],
//        elevation: 0.0,
        actions: <Widget>[
          FlatButton(
              onPressed: () async {
                await _auth.signOut();
              },
              child: Row(
                children: [
                  Text(
                    'Logout ',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  Icon(
                    Icons.login_outlined,
                    color: Colors.white,
                  ),
                ],
              )),
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              GestureDetector(
//                splashColor: Colors.amber,
                onTap: () {
                  Navigator.pushNamed(context, '/view_med_report');
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    gradient: new LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                      colors: [
                        Color.fromARGB(255, 0, 146, 255),
                        Color.fromARGB(255, 27, 187, 255)
                      ],
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(1, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  height: 150,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                          child: Icon(
                        Icons.assignment,
                        size: 60,
                        color: Colors.white,
                      )),
                      Text(
                        'Medical Report',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w900,
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 20.0,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/Food_recipe');
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    gradient: new LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.topRight,
                      colors: [
                        Color.fromARGB(255, 0, 146, 255),
                        Color.fromARGB(255, 27, 187, 255)
                      ],
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(1, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  height: 150,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                          child: Icon(
                        Icons.local_dining,
                        size: 60,
                        color: Colors.white,
                      )),
                      Text(
                        'Food recipe',
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                            fontWeight: FontWeight.w900),
                      ),
                      SizedBox(
                        height: 20.0,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
