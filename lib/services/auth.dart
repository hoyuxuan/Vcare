import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/services/database.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create user object based on FirebaseUser
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  //auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
//        .map((FirebaseUser user) => _userFromFirebaseUser(user));
        .map(_userFromFirebaseUser); //same as comment above ^
  }



  //Sign in anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //reset password
  Future sendPasswordResetEmail(String email) async {
    try {
       _auth.sendPasswordResetEmail(email: email);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Sign in w email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }


  //register w email & password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      List<dynamic> list = new List<dynamic>();
      list.add('N/A');
      var dateTime = Timestamp.now();

      //create a new document for the user with uid
      await DatabaseService(uid: user.uid).updateBMI('N/A',0.0,0.0);
      await DatabaseService(uid: user.uid).updateBP('N/A',0,0,0);
      await DatabaseService(uid: user.uid).updateBG('N/A',0,"N/A");
      await DatabaseService(uid: user.uid).updateBPList('N/A',0,0,0,dateTime);
      await DatabaseService(uid: user.uid).updateBMIList('N/A',0.0,0.0,dateTime);
      await DatabaseService(uid: user.uid).updateBGList('N/A',0,'N/A',dateTime);
      await DatabaseService(uid: user.uid).updateTag(list);
      await DatabaseService(uid: user.uid).updateDisease(false, false, false, false, false, false, false, false, false, false);
      await DatabaseService(uid: user.uid).updateAllergy(false, false, false, false, false, false, false, false, false, false, false,false);


      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }


  //Sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
