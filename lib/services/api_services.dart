
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:vcare/models/meal_plan_model.dart';
import 'package:vcare/models/recipe_model.dart';

class APIService{
APIService._instantiate();

static final APIService instance = APIService._instantiate();
final String _baseUrl = 'api.spoonacular.com';
static const String API_KEY = '2acb14dcc4844629ae2955f8e1221a8c';

// Generate Meal Plan
  // Here we filter out intolerance List from the meal plan
Future<MealPlan> generateMealPlan({int targetCalories, String diet, String intolerances}) async {
  if (diet == 'None') diet = '';
  Map<String, String> parameters = {
    'timeFrame': 'day',
    'targetCalories': targetCalories.toString(),
    'diet': diet,
    'intolerances': intolerances,
    'apiKey': API_KEY,
  };
  Uri uri = Uri.https(_baseUrl, '/recipes/mealplans/generate', parameters,);
  print(uri.toString());
  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: 'application/json',
  };

  //Get data from meal plan
  try {
    var response = await http.get(uri, headers: headers);
    Map<String, dynamic> data = json.decode(response.body);
    MealPlan mealPlan = MealPlan.fromMap(data);
    return mealPlan;
  } catch (err) {
    throw err.toString();
  }
}

// Recipe Info
Future<Recipe> fetchRecipe(String id) async {
  Map<String, String> parameters = {
    'includeNutrition': 'false',
    'apiKey': API_KEY,
  };
  Uri uri = Uri.https(
    _baseUrl,
    '/recipes/$id/information',
    parameters,
  );
  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: 'application/json',
  };

  try {
    var response = await http.get(uri, headers: headers);
    Map<String, dynamic> data = json.decode(response.body);
    Recipe recipe = Recipe.fromMap(data);
    return recipe;
  } catch (err) {
    throw err.toString();
  }
}
}
