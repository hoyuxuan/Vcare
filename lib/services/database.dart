import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:vcare/models/user.dart';

class DatabaseService {
  final String uid;
  // final Timestamp datetime;

  DatabaseService({this.uid});

  //collection reference
  final CollectionReference healthTagCollection =
      Firestore.instance.collection('Tag');
  final CollectionReference userDataCollection =
      Firestore.instance.collection('UserData');
  final CollectionReference lowFatRecipeDataCollection = Firestore.instance
      .collection('Recipe')
      .document('Z5u1Pg3tXsVFp4kfAhPW')
      .collection('LowFat');

  final CollectionReference fireData =
      Firestore.instance.collection('UserData');

  Future updateUserData(num height, num weight, Timestamp dateTime) async {
    return await fireData
        .document(uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BMI_List')
        .document(dateTime.toString())
        .setData({
      'height': height,
      'weight': weight,
    });
  }

  // List<BMI> _bmiListFromSnapshot(QuerySnapshot snapshot){
  //   return snapshot.documents.map((doc){
  //     return BMI(
  //       height:doc.data['height']?? 0,
  //       weight:doc.data['weight']?? 0,
  //     );
  //   }).toList();
  // }
  //
  // Stream<List<BMI>> get bmis {
  //   return fireData.snapshots().map(_bmiListFromSnapshot);
  // }
  Future updateBG(String bgTag, num bg, String mealtime) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('BG')
        .setData({
      'bgTag': bgTag,
      'bg': bg,
      'mealtime': mealtime,
    });
  }

  Future updateBP(String hbpTag, num sys, num dia, num pul) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('BP')
        .setData({
      'hbpTag': hbpTag,
      'sys': sys,
      'dia': dia,
      'pul': pul,
    });
  }

  Future updateBPList(
      String hbpTag, num sys, num dia, num pul, Timestamp dateTime) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BP_List')
        .document(dateTime.toString())
        .setData({
      'hbpTag': hbpTag,
      'sys': sys,
      'dia': dia,
      'pul': pul,
      'dateTime': dateTime
    });
  }

  Future updateBMI(String bmiTag, num height, num weight) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('BMI')
        .setData({
      'bmiTag': bmiTag,
      'height': height,
      'weight': weight,
    });
  }

  Future updateBMIList(
      String bmiTag, num height, num weight, Timestamp dateTime) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BMI_List')
        .document(dateTime.toString())
        .setData({
      'bmiTag': bmiTag,
      'height': height,
      'weight': weight,
      'dateTime': dateTime
    });
  }

  Future updateBGList(
      String bgTag, num bg,String mealtime, Timestamp dateTime) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BG_List')
        .document(dateTime.toString())
        .setData({
      'bgTag': bgTag,
      'bg': bg,
      'mealtime': mealtime,
      'dateTime': dateTime
    });
  }
  Future updateTag(List<dynamic> bmiTag) async {
    return await healthTagCollection.document(uid).setData({'Tags': bmiTag});
  }

  Future updateDisease(
      bool alzheimer,
      bool arthritis,
      bool asthma,
      bool cancer,
      bool diabetes,
      bool hbp,
      bool hc,
      bool heartDisease,
      bool obesity,
      bool osteoporosis) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('Diseases')
        .setData({
      'Arthritis': arthritis,
      'Asthma': asthma,
      'Alzheimer': alzheimer,
      'Cancer': cancer,
      'Diabetes': diabetes,
      'High Blood Pressure (Hypertension)': hbp,
      'High Cholesterol': hc,
      'Heart Disease': heartDisease,
      'Obesity': obesity,
      'Osteoporosis': osteoporosis,
    });
  }

  Future updateAllergy(
    bool Dairy,
    bool Egg,
    bool Gluten,
    bool Grain,
    bool Peanut,
    bool Seafood,
    bool Sesame,
    bool Shellfish,
    bool Soy,
    bool Sulfite,
    bool Tree_Nut,
    bool Wheat,
  ) async {
    return await userDataCollection
        .document(uid)
        .collection('med_report')
        .document('Allergies')
        .setData({
      'Dairy': Dairy,
      'Egg': Egg,
      'Gluten': Gluten,
      'Grain': Grain,
      'Peanut': Peanut,
      'Seafood': Seafood,
      'Sesame': Sesame,
      'Shellfish': Shellfish,
      'Soy': Soy,
      'Sulfite': Sulfite,
      'Tree Nut': Tree_Nut,
      'Wheat': Wheat,
    });
  }

  //--------------------------------------------------------------------------------------
// Load from snapshot

  //    HBP from snapshot
  UserDataBG _userBGFromSnapshot(DocumentSnapshot snapshot) {
    return UserDataBG(
      uid: uid,
      bgTag: snapshot.data['bgTag'],
      bg: snapshot.data['bg'],
      mealtime: snapshot.data['mealtime'],
    );
  }

//    HBP from snapshot
  UserDataBP _userBPFromSnapshot(DocumentSnapshot snapshot) {
    return UserDataBP(
      uid: uid,
      hbpTag: snapshot.data['hbpTag'],
      sys: snapshot.data['sys'],
      dia: snapshot.data['dia'],
      pul: snapshot.data['pul'],
    );
  }

  //user bmi from snapshot
  UserDataBMI _userBMIFromSnapshot(DocumentSnapshot snapshot) {
    return UserDataBMI(
      uid: uid,
      height: snapshot.data['height'],
      weight: snapshot.data['weight'],
      bmiTag: snapshot.data['bmiTag'],
    );
  }


  //list disease
  UserDisease _diseaseListFromSnapshot(DocumentSnapshot snapshot) {
    return UserDisease(
      alzheimer: snapshot.data['Alzheimer'],
      arthritis: snapshot.data['Arthritis'],
      asthma: snapshot.data['Asthma'],
      cancer: snapshot.data['Cancer'],
      diabetes: snapshot.data['Diabetes'],
      hbp: snapshot.data['High Blood Pressure (Hypertension)'],
      hc: snapshot.data['High Cholesterol'],
      heartDisease: snapshot.data['Heart Disease'],
      obesity: snapshot.data['Obesity'],
      osteoporosis: snapshot.data['Osteoporosis'],
    );
  }

  //list allergy
  UserAllergy _allergyListFromSnapshot(DocumentSnapshot snapshot) {
    return UserAllergy(
        Dairy: snapshot.data['Dairy'],
        Egg: snapshot.data['Egg'],
        Gluten: snapshot.data['Gluten'],
        Grain: snapshot.data['Grain'],
        Peanut: snapshot.data['Peanut'],
        Seafood: snapshot.data['Seafood'],
        Sesame: snapshot.data['Sesame'],
        Shellfish: snapshot.data['Shellfish'],
        Soy: snapshot.data['Soy'],
        Sulfite: snapshot.data['Sulfite'],
        Tree_Nut: snapshot.data['Tree Nut'],
        Wheat: snapshot.data['Wheat']);
  }

  //---------------------------------------------------------------------------------------
  // Getter Stream
  //get HBP record stream
  Stream<UserDataBP> get userBP {
    return userDataCollection
        .document(uid)
        .collection('med_report')
        .document('BP')
        .snapshots()
        .map(_userBPFromSnapshot);
  }

  //get BGlucose record stream
  Stream<UserDataBG> get userBG {
    return userDataCollection
        .document(uid)
        .collection('med_report')
        .document('BG')
        .snapshots()
        .map(_userBGFromSnapshot);
  }

  //get BMI record stream
  Stream<UserDataBMI> get userBMI {
    return userDataCollection
        .document(uid)
        .collection('med_report')
        .document('BMI')
        .snapshots()
        .map(_userBMIFromSnapshot);
  }

  // Stream<UserDataBMI> get userBMIList {
  //   return userDataCollection
  //       .document(uid)
  //       .collection('med_report')
  //       .document('UserRecords')
  //   .collection('BMI_List').document(datetime.toString())
  //       .snapshots()
  //       .map(_userBMIListFromSnapshot);
  // }

  //get Disease stream
  Stream<UserDisease> get userDisease {
    return userDataCollection
        .document(uid)
        .collection('med_report')
        .document('Diseases')
        .snapshots()
        .map(_diseaseListFromSnapshot);
  }

  //get Allergy Stream
  Stream<UserAllergy> get userAllergy {
    return userDataCollection
        .document(uid)
        .collection('med_report')
        .document('Allergies')
        .snapshots()
        .map(_allergyListFromSnapshot);
  }


//Delete data



// // ignore: missing_return
// Stream<List<Recipe>> getFilteredRecipe(List tag) {
//   //give stream of query snapshot
//   if (tag.length == 1) {
//     return Firestore.instance.collection('Recipe').where(
//         "tag",whereIn:[tag[0]]
//     ).snapshots().map((snapshot) =>
//     //convert snapshot to obj
//     snapshot.documents.map((doc) =>
//         Recipe.fromMap(doc.data, doc.documentID),
//     ).toList(),
//     );
//   }
//    if (tag.length == 2) {
//      return Firestore.instance.collection('Recipe').where(
//          "tag",whereIn:[tag[0],tag[1]]
//      ).snapshots().map((snapshot) =>
//      //convert snapshot to obj
//      snapshot.documents.map((doc) =>
//          Recipe.fromMap(doc.data, doc.documentID),
//      ).toList(),
//      );
//    }
//    if (tag.length == 3) {
//      return Firestore.instance.collection('Recipe').where(
//          "tag",whereIn:[tag[0],tag[1],tag[2]]
//      ).snapshots().map((snapshot) =>
//      //convert snapshot to obj
//      snapshot.documents.map((doc) =>
//          Recipe.fromMap(doc.data, doc.documentID),
//      ).toList(),
//      );
//    }
//  }

//get Tag stream
//  Stream<List<Recipe>>get lowFatRecipe {
//    return Firestore.instance.collection('Recipe').document('Z5u1Pg3tXsVFp4kfAhPW').collection('LowFat').snapshots().map(_recipeListFromSnapshot);
//  }

//get Low Fat recipe stream
//  Stream<List<Recipe>>get lowFatRecipe {
//    return Firestore.instance.collection('Recipe').document('Z5u1Pg3tXsVFp4kfAhPW').collection('LowFat').snapshots().map(_recipeListFromSnapshot);
//  }
//  //get high Protein recipe stream
//  Stream<List<Recipe>>get HighProteinRecipe {
//    return Firestore.instance.collection('Recipe').document('Z5u1Pg3tXsVFp4kfAhPW').collection('HighProtein').snapshots().map(_recipeListFromSnapshot);
//  }
//  //get High Blood Pressure recipe stream
//  Stream<List<Recipe>>get HBPRecipe {
//    return Firestore.instance.collection('Recipe').document('Z5u1Pg3tXsVFp4kfAhPW').collection('HighBP').snapshots().map(_recipeListFromSnapshot);
//  }
//  //get Low Blood Pressure recipe stream
//  Stream<List<Recipe>>get LBPRecipe {
//    return Firestore.instance.collection('Recipe').document('Z5u1Pg3tXsVFp4kfAhPW').collection('LowBP').snapshots().map(_recipeListFromSnapshot);
//  }

}
