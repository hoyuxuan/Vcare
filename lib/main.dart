import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vcare/Pages/MedicalReport/viewBGTrend.dart';
import 'package:vcare/Pages/authenticate/wrapper.dart';
import 'package:vcare/services/auth.dart';
import 'package:vcare/Pages/MedicalReport/medical_report.dart';
import 'package:vcare/models/user.dart';
import 'package:vcare/Pages/Food/search_screen.dart';
import 'package:vcare/Pages/MedicalReport/viewBMITrend.dart';
import 'package:vcare/Pages/MedicalReport/viewBPTrend.dart';


void main() => runApp(Vcare());

class Vcare extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        home: Wrapper(),
        routes: {
          '/wrapper': (context) => Wrapper(),
          '/view_med_report': (context) => MedicalReport(),
          '/Food_recipe': (context) => SearchScreen(),
          '/viewBMITrend':(context) => viewBMITrend(),
          '/viewBPTrend':(context) => viewBPTrend(),
          '/viewBGTrend':(context) => viewBGTrend(),
        },
      ),
    );
  }
}
